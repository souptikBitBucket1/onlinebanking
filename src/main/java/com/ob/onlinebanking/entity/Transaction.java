package com.ob.onlinebanking.entity;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "debit_account_id")
    private User debitedUserAccount;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "credit_account_id")
    private User creditedUserAccount;

    @Column(name = "amount")
    private int amount;

    @Column(name = "timestamp")
    private Timestamp timeStamp;

    public Transaction() {
        super();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getDebitedUserAccount() {
        return debitedUserAccount;
    }

    public void setDebitedUserAccount(User debitedUserAccount) {
        this.debitedUserAccount = debitedUserAccount;
    }

    public User getCreditedUserAccount() {
        return creditedUserAccount;
    }

    public void setCreditedUserAccount(User creditedUserAccount) {
        this.creditedUserAccount = creditedUserAccount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }
}
