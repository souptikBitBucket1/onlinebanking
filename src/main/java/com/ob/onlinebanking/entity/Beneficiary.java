package com.ob.onlinebanking.entity;

import javax.persistence.*;

@Entity
@Table(name = "beneficiary")
public class Beneficiary {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "beneficiary_id")
    private User benefeciaryUser;

    @Column(name = "transfer_limit")
    private int transferLimit;

    public Beneficiary() {
        super();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getBenefeciaryUser() {
        return benefeciaryUser;
    }

    public void setBenefeciaryUser(User benefeciaryUser) {
        this.benefeciaryUser = benefeciaryUser;
    }

    public int getTransferLimit() {
        return transferLimit;
    }

    public void setTransferLimit(int transferLimit) {
        this.transferLimit = transferLimit;
    }
}
