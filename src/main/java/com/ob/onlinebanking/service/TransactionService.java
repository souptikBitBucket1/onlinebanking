package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.Transaction;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface TransactionService {
    public CustomResponse doTransfer(long userAccountNo, long beneficiaryAccountNo, int amount);
    public Transaction addTransaction(long userAccountNo, long beneficiaryAccountNo, int amount);
    public Page<Transaction> getTransactionsByuserId(long id, Pageable pageable);
}
