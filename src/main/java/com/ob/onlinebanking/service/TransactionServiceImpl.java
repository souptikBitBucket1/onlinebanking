package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.Transaction;
import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.repository.TransactionRepository;
import com.ob.onlinebanking.repository.UserRepository;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("TransactionService")
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @Override
    public CustomResponse doTransfer(long userAccountNo, long beneficiaryAccountNo, int amount) {

        CustomResponse customResponse = null;

        //checking whether balance is low
        if(this.userService.getUserById(userAccountNo).getBalance() < amount){
            customResponse = new CustomResponse("error", "Low balance", "", "");
            return customResponse;
        }
            //transaction procedure
            try {
                this.userService.updateUserAndBeneficiaryBalance(userAccountNo, beneficiaryAccountNo, amount);
            } catch (Exception e){
                customResponse = new CustomResponse("error", "Exception occurred", "", e);
                return customResponse;
            }


            //Add a transaction entry in Transaction table
            Transaction transactionResult;
            try{
                transactionResult = this.addTransaction(userAccountNo, beneficiaryAccountNo, amount);
                customResponse = new CustomResponse("success", "Successfully add an entry into Transaction Table", "", transactionResult);
            }catch(Exception e){
                customResponse = new CustomResponse("error", "Failed to add an entry into Transaction Table", "", e);
            }

            return customResponse;
    }

    @Override
    public Transaction addTransaction(long userAccountNo, long beneficiaryAccountNo, int amount) {

        User userAccount = this.userRepository.findUserById(userAccountNo);
        User beneficiaryAccount = this.userRepository.findUserById(beneficiaryAccountNo);

        Transaction transaction = new Transaction();
        transaction.setDebitedUserAccount(userAccount);
        transaction.setCreditedUserAccount(beneficiaryAccount);
        transaction.setAmount(amount);

        return this.transactionRepository.save(transaction);
    }

    @Override
    public Page<Transaction> getTransactionsByuserId(long id, Pageable pageable) {
        return this.transactionRepository.findByDebitedUserAccount_IdOrCreditedUserAccount_IdOrderByTimeStampDesc(id, id, pageable);
    }
}
