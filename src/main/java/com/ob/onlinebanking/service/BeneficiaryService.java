package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.Beneficiary;
import com.ob.onlinebanking.utils.CustomResponse;

import java.util.List;

public interface BeneficiaryService {

    public List<Beneficiary> getAllBeneficiariesByUserId(long userId);
    public CustomResponse addBeneficiary(long userId, long beneficiaryUserId, int transferLimit, String beneficiaryName);
    public void delete(long id);
}
