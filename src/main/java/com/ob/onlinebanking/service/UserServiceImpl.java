package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> getUserByEmailAndPassword(String email, String password) {
        return this.userRepository.findUserByEmailAndPassword(email, password);
    }

    @Override
    public User getUserById(long id) {
        return this.userRepository.findUserById(id);
    }

    @Override
    public User addUser(User user) {
        return this.userRepository.save(user);
    }

    @Override
    public User getBeneficiaryUser(long id, String name) {
        return this.userRepository.findUserByIdAndName(id, name);
    }

    @Override
    public void updateUserAndBeneficiaryBalance(long userAccountNo, long beneficiaryAccountNo, int amount) {
        this.userRepository.updateUserAndBeneficiaryBalance(userAccountNo, beneficiaryAccountNo, amount);
    }

    @Override
    public List<User> findUsersIfBalanceGreaterThan(int amount) {
        return this.userRepository.findByBalanceGreaterThan(amount);
    }
}
