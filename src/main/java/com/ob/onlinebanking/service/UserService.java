package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.User;

import java.util.List;

public interface UserService {

    public List<User> getUserByEmailAndPassword(String email, String password);
    public User getUserById(long id);
    public User getBeneficiaryUser(long id, String name);
    public User addUser(User user);
    public void updateUserAndBeneficiaryBalance(long userAccountNo, long beneficiaryAccountNo, int amount);
    List<User> findUsersIfBalanceGreaterThan(int amount);
}
