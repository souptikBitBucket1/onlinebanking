package com.ob.onlinebanking.service;

import com.ob.onlinebanking.entity.Beneficiary;
import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.repository.BeneficiaryRepository;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("beneficiaryService")
public class BeneficiaryServiceImpl implements BeneficiaryService {

    @Autowired
    private BeneficiaryRepository beneficiaryRepository;
    @Autowired
    private UserService userService;

    @Override
    public List<Beneficiary> getAllBeneficiariesByUserId(long userId) {
        //return this.beneficiaryRepository.findAllByUserId(userId);
        return this.beneficiaryRepository.getAllBeneficiaries(userId);
    }

    @Override
    public CustomResponse addBeneficiary(long userId, long beneficiaryUserId, int transferLimit, String beneficiaryName) {

        //check if both account Nos are same
        if(userId == beneficiaryUserId){
            return new CustomResponse("error", "Same Account No., Can not perform transaction", "", "");
        }

        //get user by Id
        User user = this.userService.getUserById(userId);
        //get beneficiaryUser by Id
        User beneficiaryUser = this.userService.getBeneficiaryUser(beneficiaryUserId, beneficiaryName);

        //checking whether userAccount or BeneficiaryUserAccount exists or not
        if(user == null && beneficiaryUser == null){
            return new CustomResponse("error", "userAndBeneficiaryNotFound", "", "");
        }else if(beneficiaryUser == null){
            return new CustomResponse("error", "beneficiaryNotFound", "", "");
        }else if(user == null){
            return new CustomResponse("error", "userNotFound", "", "");
        }

        //perform add beneficiary
        Beneficiary beneficiary = new Beneficiary();
        beneficiary.setUser(user);
        beneficiary.setBenefeciaryUser(beneficiaryUser);
        beneficiary.setTransferLimit(transferLimit);

        Beneficiary newBeneficiary;
        try{
            newBeneficiary = this.beneficiaryRepository.save(beneficiary);
            return new CustomResponse("success", "", "", newBeneficiary);
        }catch (Exception e){
            //return new ResponseEntity<>("sqlException", HttpStatus.EXPECTATION_FAILED);
            return new CustomResponse("", "", "", "");
        }
    }

    @Override
    public void delete(long id) {
        this.beneficiaryRepository.delete(id);
    }
}
