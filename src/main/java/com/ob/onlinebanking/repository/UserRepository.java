package com.ob.onlinebanking.repository;

import com.ob.onlinebanking.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    public List<User> findUserByEmailAndPassword(String email, String password);
    public User findUserById(long id);
    public User findUserByIdAndName(long id, String name);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value =  " UPDATE user SET balance = CASE id " +
                    " WHEN :userAccountNo THEN balance - (:amount) " +
                    " WHEN :beneficiaryAccountNo THEN balance + (:amount) " +
                    " ELSE balance END " +
                    " WHERE id IN (:userAccountNo, :beneficiaryAccountNo) ", nativeQuery = true)
    public void updateUserAndBeneficiaryBalance(@Param("userAccountNo") long userAccountNo,
                           @Param("beneficiaryAccountNo") long beneficiaryAccountNo,
                           @Param("amount") int amount);

    List<User> findByBalanceGreaterThan(int amount);
}
