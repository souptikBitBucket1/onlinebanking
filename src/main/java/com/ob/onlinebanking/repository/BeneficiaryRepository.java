package com.ob.onlinebanking.repository;

import com.ob.onlinebanking.entity.Beneficiary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface BeneficiaryRepository extends JpaRepository<Beneficiary, Long>{
    public List<Beneficiary> findAllByUserId(int userId);

    @Query(value = "select * from beneficiary where user_id = :userId", nativeQuery = true)
    public List<Beneficiary> getAllBeneficiaries(@Param("userId") long userId);
}
