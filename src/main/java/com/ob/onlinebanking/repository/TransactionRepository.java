package com.ob.onlinebanking.repository;

import com.ob.onlinebanking.entity.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    public Page<Transaction> findByDebitedUserAccount_IdOrCreditedUserAccount_IdOrderByTimeStampDesc(long debitUserId, long creditUserId, Pageable pageable);
}
