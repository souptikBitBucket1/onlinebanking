package com.ob.onlinebanking.utils;

public class CustomResponse {
    private String status;
    private String msg;
    private Object jId;
    private Object data;

    public CustomResponse(String status, String msg, Object jId, Object data) {
        this.status = status;
        this.msg = msg;
        this.jId = jId;
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getjId() {
        return jId;
    }

    public void setjId(Object jId) {
        this.jId = jId;
    }
}
