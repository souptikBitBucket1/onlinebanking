package com.ob.onlinebanking.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/")
public class TestController{

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> test(HttpServletRequest request){
        HttpSession session = request.getSession();
        return new ResponseEntity<>(session.getId(), HttpStatus.OK);
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public ResponseEntity<?> test1(HttpServletRequest request){
        HttpSession session = request.getSession();
        return new ResponseEntity<>(session.getId(), HttpStatus.OK);
    }

    @RequestMapping(value = "/tst", method = RequestMethod.POST)
    public ResponseEntity<?> tst(@RequestBody Map<String, Object> body){

        String name = body.get("name").toString();

        return new ResponseEntity<>(name + " Api call", HttpStatus.OK);
    }
}