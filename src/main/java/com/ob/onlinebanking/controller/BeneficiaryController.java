package com.ob.onlinebanking.controller;

import com.ob.onlinebanking.entity.Beneficiary;
import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.service.BeneficiaryService;
import com.ob.onlinebanking.service.UserService;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/beneficiary")
public class BeneficiaryController {

    @Autowired
    private BeneficiaryService beneficiaryService;
    @Autowired
    private UserService userService;



    /**
     * Add a beneficiary for a particular userAccount
     *
     * Map<String, Object> requestBody = [  "userAccountNo" = 12,
     *                                      "beneficiaryAccountNo" = 13,
     *                                      "transferLimit" = 10000
     *                                  ]
     *
     * */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> addBeneficiary(@RequestBody Map<String, Object> requestBody){

        //fetching data from payload/request body
        long userId = Long.parseLong(requestBody.get("userAccountNo").toString());
        long beneficiaryUserId = Long.parseLong(requestBody.get("beneficiaryAccountNo").toString());
        String beneficiaryName = requestBody.get("beneficiaryName").toString();
        int transferLimit = 10000;

        /* Check whether transfer limit is valid*/
        if(requestBody.get("transferLimit") != null){
            try{
                transferLimit = Integer.parseInt(requestBody.get("transferLimit").toString());
            }catch (NumberFormatException e){
                System.out.print(e);
            }
        }

        //perform add beneficiary
        CustomResponse customResponse = beneficiaryService.addBeneficiary(userId, beneficiaryUserId, transferLimit, beneficiaryName);
        return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }

    @GetMapping(value = "/{userId}")
    public ResponseEntity<?> getBeneficiariesByUserId(@PathVariable("userId") String userId){
        List<Beneficiary> beneficiaries = this.beneficiaryService.getAllBeneficiariesByUserId(Integer.parseInt(userId));
        return new ResponseEntity<>(beneficiaries, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteBeneficiary(@PathVariable("id") String id){
        this.beneficiaryService.delete(Long.parseLong(id));
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
