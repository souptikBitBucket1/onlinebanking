package com.ob.onlinebanking.controller;

import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.service.UserService;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<?> getUserByEmailAndPassword(HttpServletRequest request,
                                                       @RequestParam("email") String email,
                                                       @RequestParam("password") String password){


        List<User> users = this.userService.getUserByEmailAndPassword(email, password);
        System.out.print("is user empty" + users.isEmpty());
        if(users.isEmpty()){
            CustomResponse customResponse = new CustomResponse("error", "Login failed", "", "");
            return new ResponseEntity(customResponse, HttpStatus.NO_CONTENT);
        }else{

            HttpSession session = request.getSession();

            CustomResponse customResponse = new CustomResponse("success", "Login successfull", session.getId(), users.get(0));
            return new ResponseEntity<>(customResponse, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getUserById(HttpServletRequest request, @PathVariable("id") String id){
        User user = this.userService.getUserById(Long.parseLong(id.toString()));

        HttpSession session = request.getSession();

        if(user != null){
            CustomResponse customResponse = new CustomResponse("success", "Data fetched successfully", session.getId(), user);
            return new ResponseEntity<>(customResponse, HttpStatus.OK);
        }else{
            CustomResponse customResponse = new CustomResponse("error", "Failed to fetch data", session.getId(), "");
            return new ResponseEntity<>(customResponse, HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/balanceGreaterThan/{amount}", method = RequestMethod.GET)
    public ResponseEntity<?> getUsersIfBalanceGreaterThanSomeAmount(@PathVariable("amount") String amount){
        List<User> users = userService.findUsersIfBalanceGreaterThan(Integer.parseInt(amount));
        if(!users.isEmpty()){
            CustomResponse customResponse = new CustomResponse("success", "Data fetched successfully", "", users);
            return new ResponseEntity<>(customResponse, HttpStatus.OK);
        }else{
            CustomResponse customResponse = new CustomResponse("error", "Failed to fetch data", "", users);
            return new ResponseEntity<>(customResponse, HttpStatus.NO_CONTENT);
        }
    }
}
