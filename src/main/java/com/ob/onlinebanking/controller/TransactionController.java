package com.ob.onlinebanking.controller;

import com.ob.onlinebanking.entity.Transaction;
import com.ob.onlinebanking.entity.User;
import com.ob.onlinebanking.service.TransactionService;
import com.ob.onlinebanking.service.UserService;
import com.ob.onlinebanking.utils.CustomResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;


    /**
     * Calls doTransfer() method of transactionService
    * Map<String, Object> transactionPayload = ["userAccountNo" = 12,
    *                                           "beneficiaryAccountNo" = 13
    *                                           "transferAmount" = 10000];
    * */
    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity<?> doTransfer(@RequestBody Map<String, Object> transactionPayload){

			//fetching payload
            long userAccountNo = Long.parseLong(transactionPayload.get("userAccountNo").toString());
            long beneficiaryAccountNo = Long.parseLong(transactionPayload.get("beneficiaryAccountNo").toString());
            int transferAmount = Integer.parseInt(transactionPayload.get("transferAmount").toString());

            //Transaction service
            CustomResponse customResponse = this.transactionService.doTransfer(userAccountNo, beneficiaryAccountNo, transferAmount);
            return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }



    /**
     * get All From Transaction History for a particular user.
     *
     *
     * */
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity<?> getTransactionByuserId(@PathVariable("userId") String userId){
        Page<Transaction> transactionList = this.transactionService.getTransactionsByuserId(Long.parseLong(userId.toString()), new PageRequest(0, 10));

        CustomResponse customResponse = new CustomResponse("success", "Transaction Details fetched successfully", "", transactionList);
        return new ResponseEntity<>(customResponse, HttpStatus.OK);
    }
}
